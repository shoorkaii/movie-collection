module.exports = {
  preset: 'react-native',
  testMatch: ['**/__tests__/**/*.js'],
  setupFilesAfterEnv: ['<rootDir>/__mocks__/index.js'],
  transformIgnorePatterns: ['/node_modules/?!(react-native|react-navigation)'],
  setupFiles: ['./node_modules/react-native-gesture-handler/jestSetup.js'],
};
