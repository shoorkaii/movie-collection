import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../components/auth/Login';

const Stack = createStackNavigator();

function AuthNavigator() {
  return (
    <Stack.Navigator initialRouteName="Login">
      <Stack.Screen name={'Login'} component={Login} />
    </Stack.Navigator>
  );
}

export default AuthNavigator;
