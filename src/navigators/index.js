import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Home from '../components/home/Home';
import Launch from '../components/init/Launch';
import Login from '../components/auth/Login';
import Categories from '../components/category/Categories';
import Movies from '../components/movie/Movies';
import {useSelector} from 'react-redux';

const Stack = createStackNavigator();

function MainNavigator() {
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);

  if (isLoggedIn) {
    return (
      <Stack.Navigator initialRouteName="Launch">
        <Stack.Screen name={'Launch'} component={Launch} />
        <Stack.Screen name={'Home'} component={Home} />
        <Stack.Screen name={'Categories'} component={Categories} />
        <Stack.Screen name={'Movies'} component={Movies} />
      </Stack.Navigator>
    );
  } else {
    return (
      <Stack.Navigator initialRouteName="Launch">
        <Stack.Screen name={'Launch'} component={Launch} />
        <Stack.Screen name={'Login'} component={Login} />
      </Stack.Navigator>
    );
  }
}

export default MainNavigator;
