import React from 'react';
import {StyleSheet, TextInput} from 'react-native';
import {ELEMENT_HEIGHT, ELEMENT_WIDTH} from '../../styles/commonSizes';
import {COLOR_DARK} from '../../styles/colors';

const SimpleTextInput = (props) => {
  const {onChangeText, type = 'text', placeholder = '', ...otherProps} = props;

  const [value, setValue] = React.useState('');

  function onChangeValue(value) {
    setValue(value);
    onChangeText(value);
  }

  return (
    <TextInput
      {...otherProps}
      style={styles.textInput}
      onChangeText={(text) => onChangeValue(text)}
      value={value}
      placeholder={placeholder}
      secureTextEntry={type === 'password'}
    />
  );
};

export default SimpleTextInput;

const styles = StyleSheet.create({
  textInput: {
    height: ELEMENT_HEIGHT,
    width: ELEMENT_WIDTH,
    borderColor: COLOR_DARK,
    borderWidth: 1,
    marginBottom: 5,
  },
});
