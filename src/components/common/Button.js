import React from 'react';
import {COLOR_BLUE} from '../../styles/colors';
import {StyleSheet, Button} from 'react-native';

function SimpleButton(props) {
  const {onPress, title = 'Submit', color = COLOR_BLUE, ...otherProps} = props;

  return (
    <Button
      {...otherProps}
      style={styles.button}
      onPress={onPress}
      title={title}
      color={color}
    />
  );
}

export default SimpleButton;

const styles = StyleSheet.create({
  button: {},
});
