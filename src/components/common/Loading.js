import React from 'react';
import {COLOR_BLUE} from '../../styles/colors';
import {ActivityIndicator} from 'react-native';

/**
 * @return {null}
 */
function Loading(props) {
  const {loading = true} = props;

  return loading && <ActivityIndicator size={'large'} color={COLOR_BLUE} />;
}

export default Loading;
