import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import TextInput from '../common/TextInput';
import Button from '../common/Button';
import {COLOR_BLUE, COLOR_ORANGE} from '../../styles/colors';
import {useNavigation} from '@react-navigation/native';

function Home() {
  const navigation = useNavigation();

  const [query, setQuery] = useState('');

  return (
    <View style={styles.home}>
      <TextInput
        onChangeText={setQuery}
        placeholder={'search something'}
        maxLength={10}
      />

      <Button
        onPress={() => {
          navigation.navigate('Movies', {search: query});
        }}
        title={`search the "${query}"`}
        color={COLOR_ORANGE}
      />

      <Button
        onPress={() => {
          navigation.navigate('Categories');
        }}
        title={'Go to Categories'}
      />
    </View>
  );
}

export default Home;

const styles = StyleSheet.create({
  home: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  query: {
    fontSize: 18,
    color: COLOR_BLUE,
    margin: 10,
  },
});
