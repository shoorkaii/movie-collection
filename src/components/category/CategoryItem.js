import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {ELEMENT_HEIGHT, SCREEN_WIDTH} from '../../styles/commonSizes';
import {COLOR_DARK} from '../../styles/colors';

function CategoryItem(item, navigation) {
  const {name} = item.item;

  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate('Movies', {tags: name});
      }}
      style={styles.categoryItem}>
      <Text>{name}</Text>
    </TouchableOpacity>
  );
}

export default CategoryItem;

const styles = StyleSheet.create({
  categoryItem: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: COLOR_DARK,
    borderWidth: 0.25,
    height: ELEMENT_HEIGHT,
    width: SCREEN_WIDTH,
  },
  categoryItemText: {
    color: COLOR_DARK,
    textAlign: 'center',
  },
});
