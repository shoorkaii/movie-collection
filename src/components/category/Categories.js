import React, {useEffect} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import category from '../../store/categoryMovie';
import {shallowEqual, useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import CategoryItem from './CategoryItem';
import Loading from '../common/Loading';

function Categories() {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const categories = useSelector(
    (state) => state.category.categories,
    shallowEqual,
  );

  useEffect(() => {
    dispatch(category.actions.getCategories()).catch((error) =>
      console.log(error),
    );
  }, [dispatch]);

  return (
    <View style={styles.categories}>
      {categories.length === 0 ? (
        <Loading />
      ) : (
        <FlatList
          data={categories}
          listEmptyComponent={<Text>Categories</Text>}
          numColumns={1}
          keyExtractor={(item) => item.id.toString()}
          renderItem={(item) => CategoryItem(item, navigation)}
          removeClippedSubviews={false}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={true}
          style={styles.categoryList}
        />
      )}
    </View>
  );
}

export default Categories;

const styles = StyleSheet.create({
  categories: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  categoryList: {
    flex: 1,
  },
});
