import React, {useEffect} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import category from '../../store/categoryMovie';
import {shallowEqual, useDispatch, useSelector} from 'react-redux';
import MovieItem from './MovieItem';
import Loading from '../common/Loading';

function Movies({navigation, route}) {
  const {params} = route;
  const dispatch = useDispatch();

  const movies = useSelector(
    (state) => state.category.filteredMovies,
    shallowEqual,
  );

  useEffect(() => {
    dispatch(category.actions.getMoviesWithFilter(params)).catch((error) =>
      console.log(error),
    );
  }, [dispatch, params]);

  return (
    <View style={styles.movies}>
      {movies.length === 0 ? (
        <Loading />
      ) : (
        <FlatList
          data={movies}
          listEmptyComponent={<Text>{'Movies'}</Text>}
          numColumns={1}
          keyExtractor={(item) => item.id.toString()}
          renderItem={(item) => MovieItem(item)}
          removeClippedSubviews={false}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={true}
          style={styles.moviesList}
        />
      )}
    </View>
  );
}

export default Movies;

const styles = StyleSheet.create({
  movies: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  moviesList: {
    flex: 1,
  },
});
