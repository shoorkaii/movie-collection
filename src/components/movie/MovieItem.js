import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {COLOR_BLUE} from '../../styles/colors';
import {SCREEN_WIDTH} from '../../styles/commonSizes';
import {getDirectorName} from '../../utils/movieUtils';

function MovieItem(props) {
  const {title, rating, director} = props.item;

  return (
    <View style={styles.movieItem}>
      <Text>{`Title: ${title}`}</Text>
      <Text>{`Director: ${getDirectorName(director)}`}</Text>
      <Text>{`Rate: ${rating}`}</Text>
    </View>
  );
}

export default MovieItem;

const styles = StyleSheet.create({
  movieItem: {
    width: SCREEN_WIDTH,
    borderWidth: 0.25,
    borderColor: COLOR_BLUE,
  },
});
