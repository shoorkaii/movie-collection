import React, {useEffect, useState} from 'react';
import {useDispatch} from 'react-redux';
import {StyleSheet, Text, View} from 'react-native';
import SecureStorage from '../../utils/secureStorage';
import {UNAUTHORIZED} from '../../utils/errors';
import auth from '../../store/auth';
import Loading from '../common/Loading';

function Launch(props) {
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    SecureStorage.getToken()
      .then((token) => {
        if (token == null || token === '') {
          return Promise.reject({errorType: UNAUTHORIZED});
        } else {
          dispatch(auth.actions.setToken(token));
          props.navigation.navigate('Home');
        }
      })
      .catch((error) => {
        console.log('launch catch', error);
        props.navigation.navigate('Login');
      })
      .finally(() => setLoading(false));
  });

  return (
    <View style={styles.launch}>
      <Text style={styles.launchText}>Launch</Text>

      <Loading loading={loading} />
    </View>
  );
}

export default Launch;

const styles = StyleSheet.create({
  launch: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  launchText: {
    marginBottom: 20,
  },
});
