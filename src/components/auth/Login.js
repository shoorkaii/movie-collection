import React, {useState} from 'react';
import auth from '../../store/auth';
import {StyleSheet, Text, View} from 'react-native';
import Button from '../common/Button';
import TextInput from '../common/TextInput';
import {useDispatch} from 'react-redux';

function Login(props) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const dispatch = useDispatch();

  function login() {
    dispatch(auth.actions.login(username, password))
      .then(() => props.navigation.navigate('Home'))
      .catch((error) => {
        console.log('error', error);
      });
  }

  return (
    <View style={styles.login}>
      <Text>Login</Text>

      <TextInput onChangeText={setUsername} placeholder={'username'} />
      <TextInput
        onChangeText={setPassword}
        type={'password'}
        placeholder={'password'}
      />
      <Button onPress={login} />
    </View>
  );
}

export default Login;

const styles = StyleSheet.create({
  login: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
});
