import actions from './actions';
import reducers from './reducers';

const category = {
  actions: actions,
  reducers: reducers,
};

export default category;
