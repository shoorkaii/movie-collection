import actionTypes from '../actionTypes';
import Request from '../../api/request';
import Urls from '../../api/urls';
import {asyncActionTypeCreator} from '../../utils/getActionTypes';

const actions = {
  getCategories: () => {
    return {
      types: asyncActionTypeCreator(actionTypes.GET_CATEGORIES),
      callAPI: (state) => Request.get(state, Urls.getCategories, {}, {}, false),
    };
  },
  getMoviesWithFilter: (param) => {
    return {
      types: asyncActionTypeCreator(actionTypes.GET_FILTERED_MOVIES),
      callAPI: (state) => Request.get(state, Urls.getMovies, param, {}, false),
    };
  },
};

export default actions;
