import {combineReducers} from 'redux';
import actionTypes from '../actionTypes';
import {asyncActionTypeCreator} from '../../utils/getActionTypes';

const categories = (state = [], action) => {
  switch (action.type) {
    case asyncActionTypeCreator(actionTypes.GET_CATEGORIES)[1]:
      return action.response.results;
    case actionTypes.LOGOUT:
      return null;
    default:
      return state;
  }
};

const filteredMovies = (state = [], action) => {
  switch (action.type) {
    case asyncActionTypeCreator(actionTypes.GET_FILTERED_MOVIES)[1]:
      return action.response.results;
    case actionTypes.LOGOUT:
      return null;
    default:
      return state;
  }
};

const reducers = combineReducers({
  categories,
  filteredMovies,
});

export default reducers;
