import {combineReducers} from 'redux';
import Auth from './auth';
import Category from './categoryMovie';

const appReducer = combineReducers({
  auth: Auth.reducers,
  category: Category.reducers,
});

const rootReducer = (state, action) => appReducer(state, action);

export default rootReducer;
