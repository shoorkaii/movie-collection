import {applyMiddleware, compose, createStore} from 'redux';
import thunk from 'redux-thunk';
import {logger} from 'redux-logger';
import {isProduction} from '../utils/environment';
import {callAPIMiddleware} from '../utils/redux';
import rootReducer from './reducers';

const enhancers = [];
const middleware = [thunk, callAPIMiddleware];

if (!isProduction) {
  middleware.push(logger);
}
const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);

export default createStore(rootReducer, composedEnhancers);
