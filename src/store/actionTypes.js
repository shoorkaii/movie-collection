const actionTypes = {
  LOGIN: 'LOGIN',
  SET_TOKEN: 'SET_TOKEN',
  LOGOUT: 'LOGOUT',
  GET_CATEGORIES: 'GET_CATEGORIES',
  GET_FILTERED_MOVIES: 'GET_FILTERED_MOVIES',
};

export default actionTypes;
