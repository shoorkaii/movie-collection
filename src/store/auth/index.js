import actions from './actions';
import reducers from './reducers';

const auth = {
  actions: actions,
  reducers: reducers,
};

export default auth;
