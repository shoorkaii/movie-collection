import actionTypes from '../actionTypes';
import {makeActionCreator} from '../../utils/redux';
import Request from '../../api/request';
import Urls from '../../api/urls';
import {asyncActionTypeCreator} from '../../utils/getActionTypes';

const actions = {
  logout: makeActionCreator(actionTypes.LOGOUT),
  setToken: makeActionCreator(actionTypes.SET_TOKEN, 'token'),

  login: (username, password) => {
    return {
      types: asyncActionTypeCreator(actionTypes.LOGIN),
      shouldCallAPI: (state) => !state.auth.isLoggedIn,
      callAPI: (state) =>
        Request.post(
          state,
          Urls.login,
          {},
          {
            username,
            password,
          },
          false,
        ),
    };
  },
};

export default actions;
