import {combineReducers} from 'redux';
import actionTypes from '../actionTypes';
import SecureStorage from '../../utils/secureStorage';
import {asyncActionTypeCreator} from '../../utils/getActionTypes';

const isLoggedIn = (state = false, action) => {
  switch (action.type) {
    case asyncActionTypeCreator(actionTypes.LOGIN)[1]:
    case actionTypes.SET_TOKEN:
      return true;
    case actionTypes.LOGOUT:
      return false;
    default:
      return state;
  }
};

const token = (state = null, action) => {
  let token = null;
  switch (action.type) {
    case asyncActionTypeCreator(actionTypes.LOGIN)[1]:
      token = action.response.token;
      SecureStorage.setToken(token).catch((error) => console.log(error));
      return token;
    case actionTypes.SET_TOKEN:
      token = action.token;
      SecureStorage.setToken(token).catch((error) => console.log(error));
      return token;
    case actionTypes.LOGOUT:
      return null;
    default:
      return state;
  }
};

const reducers = combineReducers({
  isLoggedIn,
  token,
});

export default reducers;
