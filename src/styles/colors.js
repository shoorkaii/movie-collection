const COLOR_WHITE = '#ffffff';
const COLOR_GRAY = '#eceff1';
const COLOR_BLACK = '#000000';
const COLOR_DARK = '#263238';
const COLOR_BLUE = '#1f78d1';
const COLOR_ORANGE = '#f57c00';

export {
  COLOR_WHITE,
  COLOR_GRAY,
  COLOR_BLACK,
  COLOR_BLUE,
  COLOR_DARK,
  COLOR_ORANGE,
};
