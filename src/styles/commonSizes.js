import {Dimensions} from 'react-native';

//dimensions
export const SCREEN_WIDTH = Dimensions.get('window').width;
export const SCREEN_HEIGHT = Dimensions.get('window').height;

//elements size
export const ELEMENT_WIDTH = SCREEN_WIDTH * 0.55;
export const ELEMENT_HEIGHT = 45;
