export const REQUEST_FAILED = 'REQUEST_FAILED';
export const UNAUTHORIZED = 'UNAUTHORIZED';
export const PERMISSION_DENIED = 'PERMISSION_DENIED';
export const SERVER_ERROR = 'SERVER_ERROR';
export const NETWORK_ERROR = 'NETWORK_ERROR';
export const TIMEOUT = 'TIMEOUT';
export const UNKNOWN_ERROR = 'UNKNOWN_ERROR';

export const ERROR_MESSAGES = {
  [UNAUTHORIZED]: 'لطفا دوباره وارد شوید',
  [PERMISSION_DENIED]: 'شما مجاز به انجام این عملیات نیستید',
  [SERVER_ERROR]: 'در حال حاضر سرور پاسخگو نیست، دوباره تلاش کنید',
  [NETWORK_ERROR]: 'اتصال خود به اینترنت را بررسی کنید',
  [TIMEOUT]: 'پاسخی از سرور دریافت نشد',
  [UNKNOWN_ERROR]: 'لطفا دوباره تلاش کنید',
};
