const getDirectorName = (item) => {
  return item.split(': ')[1];
};

export {getDirectorName};
