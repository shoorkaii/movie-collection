const asyncActionTypeCreator = (actionType) => [
  `${actionType}_REQUEST`,
  `${actionType}_SUCCESS`,
  `${actionType}_FAILURE`,
];

export {asyncActionTypeCreator};
