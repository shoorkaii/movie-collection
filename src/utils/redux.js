export function makeActionCreator(type, ...argNames) {
  return function (...args) {
    const action = {type};
    argNames.forEach((arg, index) => {
      action[argNames[index]] = args[index];
    });
    return action;
  };
}

export function callAPIMiddleware({dispatch, getState}) {
  return (next) => (action) => {
    const {types, callAPI, shouldCallAPI = () => true, payload = {}} = action;
    const store = getState();

    if (!types) {
      return next(action);
    }

    if (
      !Array.isArray(types) ||
      types.length !== 3 ||
      !types.every((type) => typeof type === 'string')
    ) {
      return Promise.reject(Error('Expected an array of three string types.'));
    }

    if (typeof callAPI !== 'function') {
      return Promise.reject(Error('Expected callAPI to be a function.'));
    }

    if (!shouldCallAPI(store)) {
      return Promise.reject(Error('API call not allowed.'));
    }

    const [requestType, successType, failureType] = types;

    dispatch(
      Object.assign({}, payload, {
        type: requestType,
      }),
    );

    return callAPI(store)
      .then((response) => {
        dispatch(
          Object.assign({}, payload, {
            response,
            type: successType,
          }),
        );
        return response;
      })
      .catch((error) => {
        dispatch(
          Object.assign({}, payload, {
            error,
            type: failureType,
          }),
        );
        return Promise.reject(error);
      });
  };
}
