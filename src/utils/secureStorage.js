import Keychain from 'react-native-keychain';

const USERNAME = 'Token';

const SecureStorage = {
  setToken: async (token) => {
    await Keychain.setGenericPassword(USERNAME, token);
  },
  getToken: async () => {
    try {
      const credentials = await Keychain.getGenericPassword();
      if (credentials) {
        let token = credentials.password;
        if (token !== null && token !== undefined) {
          return token;
        }
      }
      return null;
    } catch (error) {
      return null;
    }
  },
  clearToken: async () => {
    await Keychain.resetGenericPassword();
  },
};

export default SecureStorage;
