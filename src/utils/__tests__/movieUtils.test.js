import {getDirectorName} from '../movieUtils';

test('director name', () => {
  const rawNames = [
    '9353: Alex Zamm',
    '8710: Uwe Boll',
    '7904: Roger Christian',
  ];
  const names = ['Alex Zamm', 'Uwe Boll', 'Roger Christian'];

  rawNames.forEach((item, index) => {
    expect(getDirectorName(item)).toBe(names[index]);
  });
});
