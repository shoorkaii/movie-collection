const Urls = {
  //urls
  baseURL: 'https://imdb.hriks.com',
  login: '/user/auth-token',
  createPerson: '/person/',
  getCategories: '/categoryMovie',
  getMovies: '/movie/',
};

export default Urls;
