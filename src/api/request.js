import axios from 'axios';

import {ERROR_MESSAGES, REQUEST_FAILED} from '../utils/errors';
import Urls from './urls';
import get from 'lodash/get';

let request = (store, url, params, method, data, loginRequired) => {
  const baseURL = Urls.baseURL;
  let headers = {
    'Content-Type': 'application/json',
  };

  if (loginRequired) {
    headers = {...headers, Authorization: 'Token ' + store.auth.token};
  }

  let request = {
    url,
    method,
    baseURL,
    headers,
    params,
  };
  if (method !== 'get') {
    request.data = data;
  }

  return axios(request)
    .then((response) => {
      if (get(response, 'status')) {
        if (get(response, 'status') >= 300) {
          return Promise.reject({response});
        }
      } else if (!response.data.scope) {
        return Promise.reject({response});
      }

      return response.data;
    })
    .catch((error) => {
      return Promise.reject({
        errorType: REQUEST_FAILED,
        errorMessage: ERROR_MESSAGES.REQUEST_FAILED,
        detail: error,
      });
    })
    .finally(() => console.log(request));
};

const Request = {
  get: (store, url, params = {}, data = {}, loginRequired = true) =>
    request(store, url, params, 'get', {}, loginRequired),
  post: (store, url, params = {}, data = {}, loginRequired = true) =>
    request(store, url, params, 'post', data, loginRequired),
  put: (store, url, params = {}, data = {}, loginRequired = true) =>
    request(store, url, params, 'put', data, loginRequired),
  delete: (store, url, params = {}, data = {}, loginRequired = true) =>
    request(store, url, params, 'delete', {}, loginRequired),
};

export default Request;
